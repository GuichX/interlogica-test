# Interlogica Test

This project solve the FE of the Pastry Test.

## Infos

- I've used `yarn` and I configured all the scripts to use it, I advice you to use it :)
- The CSS is generated with TailwindCSS
- I've added NgRx just for demostration purpose, it's overkill in a project like this one, but is only for demostration


## How to run

1. Clone the repo
2. Install dependencies

### Dev Mode

1. `yarn start`
2. In another tab or window of your terminal: `yarn run:db`

### Build

1. `yarn build`
2. In another tab or window of your terminal: `yarn run:db`
3. `yarn serve:build`


## To Do

1. Unit tests of components
2. e2e tests
3. Improve performance with `onPush` and some cleanup
4. Improve build of css with purgeCSS
