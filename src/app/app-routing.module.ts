import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AuthGuard } from './core/guards/auth.guard'

import { DessertComponent } from './core/components/dessert/dessert.component'
import { LoginComponent } from './core/components/login/login.component'

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('./feature/dashboard/dashboard.module').then((m) => m.DashboardModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: ':id',
    loadChildren: () => import('./feature/recipe/recipe.module').then((m) => m.RecipeModule),
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
    component: DessertComponent,
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
