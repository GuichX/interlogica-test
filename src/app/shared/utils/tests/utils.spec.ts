import { getDiscountedPrice, getHowMuchIsOld } from '../utils'

describe('Utils Suite Test', () => {
  describe('Discount Suite Test', () => {
    it('should return the price discounted', () => {
      expect(getDiscountedPrice(28, 80)).toEqual(5.6)
    })
  })

  describe('How much is old Suite Test', () => {
    it('should return 0 if date is the same', () => {
      expect(getHowMuchIsOld(new Date().getTime())).toEqual(0)
    })

    it('should return 3 if the dates have 3 days of difference', () => {
      expect(getHowMuchIsOld(1600520355735)).toEqual(3)
    })
  })
})
