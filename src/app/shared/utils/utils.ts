import { differenceInDays } from 'date-fns'

/**
 * Function to get the discounted price
 * @param price - The price to discount
 * @param discount - The discount to apply
 */
export const getDiscountedPrice = (price: number, discount: number): number => parseFloat((price - price * (discount / 100)).toFixed(2))

/**
 * Function to get how much older is a dessert
 * @param date - The dessert creation date
 */
export const getHowMuchIsOld = (date: number): number => differenceInDays(new Date(), new Date(date))
