import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { PricePipe } from './pipes/price.pipe'

import { ButtonComponent } from './components/button/button.component'
import { LoaderComponent } from './components/loader/loader.component'

@NgModule({
  declarations: [ButtonComponent, LoaderComponent, PricePipe],
  imports: [CommonModule],
  exports: [ButtonComponent, LoaderComponent, PricePipe],
})
export class SharedModule {}
