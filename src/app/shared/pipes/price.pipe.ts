import { Pipe, PipeTransform } from '@angular/core'

import { differenceInDays } from 'date-fns'

import { getDiscountedPrice } from '../utils/utils'

@Pipe({
  name: 'price',
})
export class PricePipe implements PipeTransform {
  transform(value: number, date: number): number {
    const now = new Date()
    const dateDifference = differenceInDays(now, new Date(date))

    let discountedPrice: number
    switch (dateDifference) {
      case 1:
        discountedPrice = getDiscountedPrice(value, 20)
        break
      case 2:
        discountedPrice = getDiscountedPrice(value, 80)
        break
      default:
        discountedPrice = value
        break
    }

    return discountedPrice
  }
}
