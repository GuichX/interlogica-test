import { PricePipe } from './price.pipe'
import { subDays } from 'date-fns'

describe('PricePipe', () => {
  let pipe
  let now

  beforeEach(() => {
    pipe = new PricePipe()
    now = new Date()
  })

  it('create an instance', () => {
    expect(pipe).toBeTruthy()
  })

  it('should return the price if date is the same', () => {
    expect(pipe.transform(28, now)).toEqual(28)
  })

  it('should return the 80% of the price if the difference between days is 1', () => {
    const yesterday = subDays(now, 1)
    expect(pipe.transform(28, yesterday)).toEqual(22.4)
  })

  it('should return the 20% of the price if the difference between days is 2', () => {
    const beforeYesterday = subDays(now, 2)
    expect(pipe.transform(28, beforeYesterday)).toEqual(5.6)
  })
})
