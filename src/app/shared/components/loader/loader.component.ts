import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'egs-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
  @Input() isVisible: boolean

  constructor() {}

  ngOnInit(): void {}
}
