import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'egs-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  /**
   * The type of the button
   */
  @Input() type: string

  /**
   * The color of the button
   */
  @Input() color: string

  /**
   * The text to display
   */
  @Input() text: string

  /**
   * If is disabled or not
   */
  @Input() disabled: boolean

  /**
   * The event to run a fn when clicked
   */
  @Output() handleOnClick = new EventEmitter<boolean>()

  constructor() {}
  /**
   * The function when clicked
   */
  onClick(): void {
    this.handleOnClick.emit(true)
  }
}
