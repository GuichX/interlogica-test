import { Component } from '@angular/core'
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'

import { AppState } from '../../../reducers'

import { userActions } from '../../store/actions'

@Component({
  selector: 'egs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  btnLoginLabel = 'Sign In'

  /**
   * The shape of the form
   */
  loginForm: FormGroup = this.fb.group({
    email: ['', Validators.required],
    pwd: ['', Validators.required],
  })

  /**
   * The email value to handle errors on template
   */
  get email(): AbstractControl {
    return this.loginForm.get('email')
  }

  /**
   * The pwd value to handle errors on template
   */
  get pwd(): AbstractControl {
    return this.loginForm.get('pwd')
  }

  constructor(private fb: FormBuilder, private store: Store<AppState>) {}

  /**
   * The function to do the login
   */
  handleLogin(): void {
    this.store.dispatch(
      userActions.getUser({
        ...this.loginForm.value,
      })
    )
  }
}
