import { Component, HostBinding } from '@angular/core'
import { Router } from '@angular/router'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'

import { userSelectors } from '../../store/selectors'
import { userActions } from '../../store/actions'

@Component({
  selector: 'egs-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  /**
   * The text of the button
   */
  btnDashboard = 'Dashboard'

  /**
   * The text ot the logout component
   */
  btnLogOut = 'Log out'

  /**
   * The classes to add to the host component to right rendering
   */
  @HostBinding('class') classes = 'bg-gray-800 p-2 mt-0 fixed w-full z-10 top-0'

  /**
   * If the user is logged or not
   */
  isUserLogged$: Observable<boolean> = this.store.pipe(select(userSelectors.selectIsUserLogged))

  /**
   * The name of the user
   */
  userName$: Observable<string> = this.store.pipe(select(userSelectors.selectUserName))

  constructor(private router: Router, private store: Store) {}

  /**
   * Function to navigate to the dashboard
   */
  handleOnClickDashboard(): void {
    this.router.navigateByUrl('/dashboard')
  }

  /**
   * Function to navigate to the homepage
   */
  handleOnClickHome(): void {
    this.router.navigateByUrl('/')
  }

  /**
   * Function to logout the user
   */
  handleLogOut(): void {
    this.store.dispatch(userActions.setUserLogout())
  }
}
