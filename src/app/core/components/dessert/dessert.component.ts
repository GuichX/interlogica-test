import { Component, OnInit } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'

import { AppState } from '../../../reducers'
import { IDessert } from '../../store/models/dessert.interface'

import { dessertsSelectors } from '../../store/selectors'

@Component({
  selector: 'egs-dessert',
  templateUrl: './dessert.component.html',
  styleUrls: ['./dessert.component.scss'],
})
export class DessertComponent implements OnInit {
  /**
   * If the app is loading the desserts
   */
  isLoadingDesserts$: Observable<boolean> = this.store.pipe(select(dessertsSelectors.selectIsLoadingDesserts))

  /**
   * The list of the desserts
   */
  dessertsList$: Observable<IDessert[]> = this.store.pipe(select(dessertsSelectors.selectDessertsList))

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}
}
