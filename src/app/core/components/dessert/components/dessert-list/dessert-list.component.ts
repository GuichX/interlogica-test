import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Router } from '@angular/router'

import { IDessert } from '../../../../store/models/dessert.interface'

import { getHowMuchIsOld } from '../../../../../shared/utils/utils'

@Component({
  selector: 'egs-dessert-list',
  templateUrl: './dessert-list.component.html',
  styleUrls: ['./dessert-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DessertListComponent {
  /**
   * The list of desserts
   */
  @Input() desserts: IDessert[]

  /**
   * If is loading the desserts
   */
  @Input() isLoadingDesserts: boolean

  constructor(private router: Router) {}

  /**
   * Function to get how much old is a dessert
   * @param date - The dessert date
   */
  getHowMuchIsOld(date: number): number {
    return getHowMuchIsOld(date)
  }

  /**
   * Function to navigate to the details
   */
  showDessertDetails(id: number): void {
    this.router.navigateByUrl(`/${id}`)
  }
}
