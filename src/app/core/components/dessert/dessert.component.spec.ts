import { ComponentFixture, TestBed } from '@angular/core/testing'

import { StoreModule } from '@ngrx/store'
import { reducers } from '../../../reducers'

import { DessertComponent } from './dessert.component'

describe('DessertComponent', () => {
  let component: DessertComponent
  let fixture: ComponentFixture<DessertComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DessertComponent],
      imports: [StoreModule.forRoot(reducers)],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(DessertComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
