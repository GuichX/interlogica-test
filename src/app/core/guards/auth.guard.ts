import { Injectable } from '@angular/core'
import { CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router'
import { Observable } from 'rxjs'

import { UserService } from '../store/services/user.service'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanLoad {
  constructor(private auth: UserService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.getIsUserLogged().pipe(
      map((isLogged) => {
        if (isLogged) {
          return true
        }

        this.router.navigateByUrl('/login')
        return false
      })
    )
  }
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.getIsUserLogged().pipe(
      map((isLogged) => {
        if (isLogged) {
          return true
        }

        this.router.navigateByUrl('/login')
        return false
      })
    )
  }
}
