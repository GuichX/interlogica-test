/**
 * The Model of a dessert
 */
export interface IDessert {
  /**
   * The id
   */
  id: number
  /**
   * The name
   */
  name: string
  /**
   * The recipe
   */
  recipe: IRecipe
  /**
   * How much
   */
  quantity: number
  /**
   * The date of preparation
   */
  createdAt: number
  /**
   * The price
   */
  price: number
  /**
   * The currency of the price
   */
  currency: string
}

/**
 * The Model of the recipe
 */
export interface IRecipe {
  /**
   * The ingredients
   */
  ingredients: IIngredients[]
}

/**
 * The Model of the ingredients
 */
export interface IIngredients {
  /**
   * The name
   */
  name: string
  /**
   * How much
   */
  quantity: number
  /**
   * The unit of measure for the quantity
   */
  'unit-of-measure': string
}
