/**
 * The User Model
 */
export interface IUser {
  /**
   * The name of the user
   */
  name: string
  /**
   * The email of the user
   */
  email: string
  /**
   * The password od the user
   */
  pwd: string
}
