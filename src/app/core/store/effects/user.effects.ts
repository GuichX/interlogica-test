import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, createEffect, ofType } from '@ngrx/effects'

import { of } from 'rxjs'
import { catchError, concatMap, map, tap } from 'rxjs/operators'

import { userActions } from '../actions'
import { UserService } from '../services/user.service'
import { IUser } from '../models/user.inferface'

/**
 * The effects for the user
 */
@Injectable()
export class UserEffects {
  /**
   * The effect to fetch the user and set it into the store
   */
  getUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.getUser),
      concatMap(({ email, pwd }) =>
        this.userService.getUser(email, pwd).pipe(
          map((user: IUser) => userActions.setUserLogin({ user: user[0] })),
          catchError((httpError) => of(userActions.getUserFailed({ httpError })))
        )
      )
    )
  )

  /**
   * The effect to inform that the user was fetched successfull
   */
  getUserSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.setUserLogin),
      map(() => userActions.getUserSuccess()),
      tap(() => this.router.navigateByUrl('/dashboard'))
    )
  )

  /**
   * The effect to logout the user
   */
  logOutUser$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userActions.setUserLogout),
        tap(() => this.router.navigateByUrl('/'))
      ),
    { dispatch: false }
  )

  constructor(private actions$: Actions, private userService: UserService, private router: Router) {}
}
