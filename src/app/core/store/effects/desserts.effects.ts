import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { Router } from '@angular/router'

import { of } from 'rxjs'
import { catchError, concatMap, map, tap } from 'rxjs/operators'

import { dessertsActions } from '../actions'
import { DessertsService } from '../services/desserts.service'
import { IDessert } from '../models/dessert.interface'

/**
 * The effects for the Desserts
 */
@Injectable()
export class DessertsEffects {
  /**
   * The effect to fetch and set the list or the api error
   */
  getDessertsList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dessertsActions.getDessertsList, dessertsActions.createDessertSuccess),
      concatMap(() =>
        this.dessertsService.getDessertsList().pipe(
          map((desserts: IDessert[]) => dessertsActions.setDessertsList({ desserts })),
          catchError((httpError) => of(dessertsActions.getDessertsFailed({ httpError })))
        )
      )
    )
  )

  /**
   * The effect to inform that desserts has been fetched successfully
   */
  getDessertsListSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dessertsActions.setDessertsList),
      map(() => dessertsActions.getDessertsSuccess())
    )
  )

  /**
   * The effect for create a dessert
   */
  createDessert$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dessertsActions.createDessert),
      concatMap(({ dessert }) =>
        this.dessertsService.createDessert(dessert).pipe(
          map(() => dessertsActions.createDessertSuccess()),
          catchError((httpError) => of(dessertsActions.createDessertFailed({ httpError })))
        )
      )
    )
  )

  createDessertSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(dessertsActions.createDessertSuccess),
        tap(() => this.router.navigateByUrl('/'))
      ),
    { dispatch: false }
  )

  /**
   * The constructor of the effect
   */
  constructor(private actions$: Actions, private dessertsService: DessertsService, private router: Router) {}
}
