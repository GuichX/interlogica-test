import { createFeatureSelector, createSelector } from '@ngrx/store'
import { AppState } from '../../../reducers'
import { userKey, UserState } from '../reducers/user.reducer'

/**
 * Function to create the selectors directly from the user state
 */
export const selectFromUserState = createFeatureSelector<AppState, UserState>(userKey)

/**
 * The selector to get the email
 */
export const selectUserEmail = createSelector(selectFromUserState, (state: UserState) => state.user?.email)

/**
 * The selector to get the name
 */
export const selectUserName = createSelector(selectFromUserState, (state: UserState) => state.user?.name)

/**
 * The selector to get if is loading or not an user
 */
export const selectIsLoadingUser = createSelector(selectFromUserState, (state: UserState) => state.isLoading)

/**
 * The selector to get the error
 */
export const selectUserError = createSelector(selectFromUserState, (state: UserState) => state.httpError)

/**
 * The selector to check if the user is logged or not
 */
export const selectIsUserLogged = createSelector(selectFromUserState, (state: UserState) => state.user !== null)
