import { dessertsSelectors } from '../'
import { DessertsState, initialState } from '../../reducers/desserts.reducer'
import { mockedDesserts } from '../../services/mocks/desserts.mock'

describe('Desserts Selectors Test Suite', () => {
  let state: DessertsState

  beforeEach(() => (state = { ...initialState }))

  it('should return the list of desserts', () => {
    expect(dessertsSelectors.selectDessertsList.projector(state)).toEqual(state.desserts)
  })

  it('should return if is loading or not', () => {
    expect(dessertsSelectors.selectIsLoadingDesserts.projector(state)).toEqual(state.isLoading)
  })

  it('should return the api error', () => {
    expect(dessertsSelectors.selectDessersError.projector(state)).toEqual(state.httpError)
  })

  it('should return the dessert filtered by id', () => {
    state.desserts = mockedDesserts

    expect(dessertsSelectors.selecDessertDetails(mockedDesserts[0].id).projector(state)).toEqual(state.desserts[0])
  })
})
