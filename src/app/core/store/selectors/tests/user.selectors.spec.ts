import { userSelectors } from '../'
import { initialState, UserState } from '../../reducers/user.reducer'
import { mockedUser } from '../../services/mocks/user.mock'

describe('User Selectors Test Suite', () => {
  let state: UserState

  beforeEach(() => (state = { ...initialState, user: mockedUser }))

  it('should return the email of the user', () => {
    expect(userSelectors.selectUserEmail.projector(state)).toEqual(state.user.email)
  })

  it('should return the name of the user', () => {
    expect(userSelectors.selectUserName.projector(state)).toEqual(state.user.name)
  })

  it('should return if is loading or not', () => {
    expect(userSelectors.selectIsLoadingUser.projector(state)).toEqual(state.isLoading)
  })

  it('should return the api error', () => {
    expect(userSelectors.selectUserError.projector(state)).toEqual(state.httpError)
  })
})
