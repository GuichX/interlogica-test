import * as dessertsSelectors from './desserts.selectors'
import * as userSelectors from './user.selectors'

export { dessertsSelectors, userSelectors }
