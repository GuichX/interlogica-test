import { createFeatureSelector, createSelector } from '@ngrx/store'
import { dessertsKey, DessertsState } from '../reducers/desserts.reducer'

import { AppState } from '../../../reducers'

/**
 * Function to create the selectors directly from the desserts state
 */
export const selectDessertsState = createFeatureSelector<AppState, DessertsState>(dessertsKey)

/**
 * The selector to get the list of available desserts
 */
export const selectDessertsList = createSelector(selectDessertsState, (state: DessertsState) => state.desserts)

/**
 * The selector to get if is loading desserts
 */
export const selectIsLoadingDesserts = createSelector(selectDessertsState, (state: DessertsState) => state.isLoading)

/**
 * The selector to get the error
 */
export const selectDessersError = createSelector(selectDessertsState, (state: DessertsState) => state.httpError)

/**
 * The selector to get a specific dessert into the list
 */
export const selecDessertDetails = (id: number) =>
  createSelector(selectDessertsState, (state: DessertsState) => state.desserts.filter((dessert) => dessert.id === id)[0])
