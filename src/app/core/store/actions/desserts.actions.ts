import { createAction, props } from '@ngrx/store'
import { HttpErrorResponse } from '@angular/common/http'

import { IDessert } from '../models/dessert.interface'

/**
 * Getting the desserts list
 */
export const getDessertsList = createAction('[Desserts] Get Desserts List')

/**
 * If the desserts get was successfull
 */
export const getDessertsSuccess = createAction('[Desserts] Get Desserts List Success')

/**
 * If the desserts get has failed
 */
export const getDessertsFailed = createAction('[Desserts] Get Desserts List Failed', props<{ httpError: HttpErrorResponse }>())

/**
 * Setting the desserts list
 */
export const setDessertsList = createAction('[Desserts] Set Dessers List', props<{ desserts: IDessert[] }>())

/**
 * Create a new dessert
 */
export const createDessert = createAction('[Desserts] Create Dessert', props<{ dessert: Partial<IDessert> }>())

/**
 * If the creation was successfull
 */
export const createDessertSuccess = createAction('[Desserts] Create Dessert Success')

/**
 * If the creation has failed
 */
export const createDessertFailed = createAction('[Desserts] Create Dessert Failed', props<{ httpError: HttpErrorResponse }>())
