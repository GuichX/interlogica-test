import * as dessertsActions from './desserts.actions'
import * as userActions from './user.actions'

export { dessertsActions, userActions }
