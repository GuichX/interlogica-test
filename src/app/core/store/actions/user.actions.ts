import { createAction, props } from '@ngrx/store'

import { IUser } from '../models/user.inferface'

import { HttpErrorResponse } from '@angular/common/http'

/**
 * Action to get an user
 */
export const getUser = createAction('[User] Get User', props<{ email: string; pwd: string }>())

/**
 * If the user ges was successfull
 */
export const getUserSuccess = createAction('[User] Get User Success')

/**
 * If the user get has failed
 */
export const getUserFailed = createAction('[User] Get User Failed', props<{ httpError: HttpErrorResponse }>())

/**
 * Log in the user
 */
export const setUserLogin = createAction('[User] Set User Logged In', props<{ user: IUser }>())

/**
 * Log out the user
 */
export const setUserLogout = createAction('[User] Set user Logged Out')
