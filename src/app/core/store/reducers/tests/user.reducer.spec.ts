import { initialState as userState, reducer } from '../user.reducer'
import { HttpErrorResponse } from '@angular/common/http'

import { IUser } from '../../models/user.inferface'

import { userActions } from '../../actions'
import { mockedUser } from '../../services/mocks/user.mock'

describe('User Recuder Test Suite', () => {
  let initialState

  beforeEach(() => {
    initialState = userState
  })

  it('should return the initial state', () => {
    const action = {} as any
    const newState = reducer(undefined, action)

    expect(newState).toEqual(initialState)
  })

  it('should set "isLoading" to true when getting an user', () => {
    const action = userActions.getUser({ email: mockedUser.email, pwd: mockedUser.pwd })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, isLoading: true })
  })

  it('should set "isLoading" to false when', () => {
    const newState = reducer(initialState, userActions.getUserSuccess)

    expect(newState).toEqual(initialState)
  })

  it('should set the error and "isLoading" to false when the api call fails', () => {
    const httpError = new HttpErrorResponse({
      error: 'Server Error',
      status: 500,
      statusText: 'Bad Request',
    })
    const action = userActions.getUserFailed({ httpError })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, httpError, isLoading: false })
  })

  it('should set the user', () => {
    const user: IUser = mockedUser
    const action = userActions.setUserLogin({ user })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, user })
  })

  it('should reset the state and logout the user', () => {
    const newState = reducer(initialState, userActions.setUserLogout)

    expect(newState).toEqual(initialState)
  })
})
