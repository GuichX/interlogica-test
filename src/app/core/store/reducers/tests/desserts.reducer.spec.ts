import { initialState as dessertsState, reducer } from '../desserts.reducer'
import { HttpErrorResponse } from '@angular/common/http'

import { IDessert } from '../../models/dessert.interface'

import { dessertsActions } from '../../actions'
import { mockedDesserts } from '../../services/mocks/desserts.mock'

describe('Desserts Reducer Test Suite', () => {
  let initialState

  beforeEach(() => {
    initialState = dessertsState
  })

  it('should return the initial state', () => {
    const action = {} as any
    const newState = reducer(undefined, action)

    expect(newState).toEqual(initialState)
  })

  it('should set "isLoading" to true when getting desserts', () => {
    const newState = reducer(initialState, dessertsActions.getDessertsList)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, isLoading: true })
  })

  it('should set "isLoading" to false when getting the desserts succed', () => {
    const newState = reducer(initialState, dessertsActions.getDessertsSuccess)

    expect(newState).toEqual(initialState)
  })

  it('should set the error and "isLoading" to false when the api call fails', () => {
    const httpError = new HttpErrorResponse({
      error: 'Server Error',
      status: 500,
      statusText: 'Bad Request',
    })
    const action = dessertsActions.getDessertsFailed({ httpError })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, isLoading: false, httpError })
  })

  it('should set the list of the desserts', () => {
    const desserts: IDessert[] = mockedDesserts
    const action = dessertsActions.setDessertsList({ desserts })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, desserts })
  })

  it('should set "isLoading" to true when creating a new dessert', () => {
    const [dessert] = mockedDesserts
    const action = dessertsActions.createDessert({ dessert })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, isLoading: true })
  })

  it('should set the error and "isLoading" to false when the create dessert api call fails', () => {
    const httpError = new HttpErrorResponse({
      error: 'Server Error',
      status: 500,
      statusText: 'Bad Request',
    })
    const action = dessertsActions.createDessertFailed({ httpError })
    const newState = reducer(initialState, action)

    expect(newState).not.toEqual(initialState)
    expect(newState).toEqual({ ...initialState, isLoading: false, httpError })
  })

  it('should set "isLoading" to false when creating the desserts succed', () => {
    const newState = reducer(initialState, dessertsActions.createDessertSuccess)

    expect(newState).toEqual(initialState)
  })
})
