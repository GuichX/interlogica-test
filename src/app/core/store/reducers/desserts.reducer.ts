import { HttpErrorResponse } from '@angular/common/http'
import { Action, createReducer, on } from '@ngrx/store'

import { IDessert } from '../models/dessert.interface'
import { dessertsActions } from '../actions'

/**
 * The key used
 */
export const dessertsKey = 'desserts'

/**
 * The Model of the state
 */
export interface DessertsState {
  desserts: IDessert[]
  httpError: HttpErrorResponse
  isLoading: boolean
}

/**
 * The initial state
 */
export const initialState: DessertsState = {
  desserts: [],
  httpError: null,
  isLoading: false,
}

/**
 * The desserts reducer
 */
export const dessertsReducer = createReducer<DessertsState, Action>(
  initialState,
  on(dessertsActions.getDessertsList, (state) => ({ ...state, isLoading: true })),
  on(dessertsActions.getDessertsSuccess, (state) => ({ ...state, isLoading: false })),
  on(dessertsActions.getDessertsFailed, (state, { httpError }) => ({ ...state, httpError, isLoading: false })),
  on(dessertsActions.setDessertsList, (state, { desserts }) => ({ ...state, desserts })),
  on(dessertsActions.createDessert, (state) => ({ ...state, isLoading: true })),
  on(dessertsActions.createDessertSuccess, (state) => ({ ...state, isLoading: false })),
  on(dessertsActions.createDessertFailed, (state, { httpError }) => ({ ...state, httpError, isLoading: false }))
)

/**
 * The reducer function for use the reducer in the root module
 * @param state - The current state
 * @param action - the action dispatched
 */
export function reducer(state: DessertsState | undefined, action: Action): DessertsState {
  return dessertsReducer(state, action)
}
