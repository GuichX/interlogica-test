import { HttpErrorResponse } from '@angular/common/http'
import { Action, createReducer, on } from '@ngrx/store'

import { userActions } from '../actions'

import { IUser } from '../models/user.inferface'

/**
 * The key used
 */
export const userKey = 'user'

/**
 * The Model of the state
 */
export interface UserState {
  httpError: HttpErrorResponse
  isLoading: boolean
  user: IUser
}

/**
 * The initial state
 */
export const initialState: UserState = {
  httpError: null,
  isLoading: false,
  user: null,
}

/**
 * The user reducer
 */
export const userReducer = createReducer<UserState, Action>(
  initialState,
  on(userActions.getUser, (state) => ({ ...state, isLoading: true })),
  on(userActions.getUserSuccess, (state) => ({ ...state, isLoading: false })),
  on(userActions.getUserFailed, (state, { httpError }) => ({ ...state, httpError, isLoading: false })),
  on(userActions.setUserLogin, (state, { user }) => ({ ...state, user })),
  on(userActions.setUserLogout, () => ({ ...initialState }))
)

/**
 * he reducer function for use the reducer in the root module
 * @param state - The current state
 * @param action - The action dispatched
 */
export function reducer(state: UserState | undefined, action: Action): UserState {
  return userReducer(state, action)
}
