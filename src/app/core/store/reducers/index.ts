import * as dessertsReducer from './desserts.reducer'
import * as userReducer from './user.reducer'

export { dessertsReducer, userReducer }
