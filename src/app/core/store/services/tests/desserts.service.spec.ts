import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { HttpClient } from '@angular/common/http'

import { environment } from '../../../../../environments/environment'

import { DessertsService } from '../desserts.service'
import { IDessert } from '../../models/dessert.interface'
import { mockedDesserts } from '../mocks/desserts.mock'

describe('Desserts Service Test Suite', () => {
  let httpClient: HttpClient
  let httpTestingController: HttpTestingController

  const baseUrl = environment.api_endpoint

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    })

    httpClient = TestBed.inject(HttpClient)
    httpTestingController = TestBed.inject(HttpTestingController)
  })

  it('should be created', () => {
    const service: DessertsService = TestBed.inject(DessertsService)
    expect(service).toBeTruthy()
  })

  it('should return the list of desserts', () => {
    const desserts: IDessert[] = mockedDesserts
    const apiCall = `${baseUrl}/desserts`

    httpClient.get<IDessert[]>(apiCall).subscribe((data: IDessert[]) => expect(data).toEqual(desserts))

    const req = httpTestingController.expectOne(apiCall)

    expect(req.request.method).toEqual('GET')

    req.flush(desserts)
  })

  afterEach(() => httpTestingController.verify())
})
