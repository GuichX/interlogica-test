import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { HttpClient } from '@angular/common/http'

import { StoreModule } from '@ngrx/store'
import { reducers } from '../../../../reducers'

import { environment } from '../../../../../environments/environment'

import { UserService } from '../user.service'
import { IUser } from '../../models/user.inferface'
import { mockedUser } from '../mocks/user.mock'

describe('User Service Test Suite', () => {
  let httpClient: HttpClient
  let httpTestingController: HttpTestingController

  const baseUrl = environment.api_endpoint

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, StoreModule.forRoot(reducers)],
    })

    httpClient = TestBed.inject(HttpClient)
    httpTestingController = TestBed.inject(HttpTestingController)
  })

  it('should be created', () => {
    const service: UserService = TestBed.inject(UserService)
    expect(service).toBeTruthy()
  })

  it('should return the user if email and password match', () => {
    const user: IUser = mockedUser
    const apiCall = `${baseUrl}/users?email=maria@pasticceria.it&pwd=maria`

    httpClient.get<IUser>(apiCall).subscribe((data: IUser) => expect(data).toEqual(user))

    const req = httpTestingController.expectOne(apiCall)

    expect(req.request.method).toEqual('GET')

    req.flush(user)
  })

  afterEach(() => httpTestingController.verify())
})
