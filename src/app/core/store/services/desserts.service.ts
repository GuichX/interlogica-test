import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

import { environment } from '../../../../environments/environment'

import { IDessert } from '../models/dessert.interface'

/**
 * The dessert service
 */
@Injectable({
  providedIn: 'root',
})
export class DessertsService {
  /**
   * The endpoint to call
   */
  private apiEndpoint = environment.api_endpoint

  /**
   * The constructor of the service
   */
  constructor(private http: HttpClient) {}

  /**
   * Function to get all the available desserts
   */
  getDessertsList(): Observable<IDessert[]> {
    return this.http.get<IDessert[]>(`${this.apiEndpoint}/desserts`)
  }

  /**
   * Function to create a dessert on the server
   */
  createDessert(dessert: Partial<IDessert>): Observable<Partial<IDessert>> {
    return this.http.post<Partial<IDessert>>(`${this.apiEndpoint}/desserts`, { ...dessert })
  }
}
