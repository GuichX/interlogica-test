import { IUser } from '../../models/user.inferface'

/**
 * A Mocked User for testing
 */
export const mockedUser: IUser = {
  name: 'Maria',
  email: 'maria@pasticceria.it',
  pwd: 'maria',
}
