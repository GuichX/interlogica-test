/**
 * A Mocked list of desserts
 */
import { IDessert } from '../../models/dessert.interface'

export const mockedDesserts: IDessert[] = [
  {
    id: 1,
    name: 'Torta Fantasiosa',
    quantity: 3,
    createdAt: 1600533551789,
    price: 28,
    currency: 'EUR',
    recipe: {
      ingredients: [
        {
          name: 'Farina',
          quantity: 100,
          'unit-of-measure': 'g',
        },
        {
          name: 'Uova',
          quantity: 3,
          'unit-of-measure': 'piece',
        },
        {
          name: 'Nutella',
          quantity: 200,
          'unit-of-measure': 'g',
        },
      ],
    },
  },
]
