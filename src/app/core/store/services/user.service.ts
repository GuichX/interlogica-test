import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { select, Store } from '@ngrx/store'

import { environment } from '../../../../environments/environment'

import { IUser } from '../models/user.inferface'
import { AppState } from '../../../reducers'

import { userSelectors } from '../selectors'

/**
 * The user service
 */
@Injectable({
  providedIn: 'root',
})
export class UserService {
  /**
   * The endpoint to call
   */
  private apiEndpoint = environment.api_endpoint

  /**
   * If the user is logged or not
   */
  isUserLogged$: Observable<boolean> = this.store.pipe(select(userSelectors.selectIsUserLogged))

  /**
   * The constructor of the service
   */
  constructor(private http: HttpClient, private store: Store<AppState>) {}

  /**
   * Function to get the user given an email and a password
   */
  getUser(email: string, pwd: string): Observable<IUser> {
    return this.http.get<IUser>(`${this.apiEndpoint}/users?email=${email}&pwd=${pwd}`)
  }

  /**
   * Function to check if an user is logged or not
   */
  getIsUserLogged(): Observable<boolean> {
    return this.isUserLogged$
  }
}
