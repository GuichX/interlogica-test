import { Component } from '@angular/core'
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'
import { dessertsActions } from '../../../../core/store/actions'
import { IDessert } from '../../../../core/store/models/dessert.interface'

@Component({
  selector: 'egs-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  /**
   * The label of the add ingredient button
   */
  addIngredientLabel = 'Add Ingredient'

  /**
   * The form group
   */
  dessertForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    quantity: ['', Validators.required],
    price: ['', Validators.required],
    ingredients: this.fb.array([this.createIngredient()], Validators.required),
  })

  /**
   * The getter of the ingredients from the form group
   */
  get ingredientsArray(): FormArray {
    return this.dessertForm.get('ingredients') as FormArray
  }

  constructor(private fb: FormBuilder, private store: Store) {}

  /**
   * Function to add a new ingredient to the array
   */
  createIngredient(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      quantity: [0, Validators.required],
      'unit-of-measure': ['', Validators.required],
    })
  }

  /**
   * Function to add a new ingredient by the ui
   */
  addIngredient(): void {
    this.ingredientsArray.push(this.createIngredient())
  }

  /**
   * Function to remove an ingredient
   */
  removeItem(i: number): void {
    this.ingredientsArray.removeAt(i)
  }

  /**
   * The function to handle the form submit
   */
  handleSubmit(): void {
    const dessert: Partial<IDessert> = {
      createdAt: new Date().getTime(),
      currency: 'EUR',
      name: this.dessertForm.value.name,
      quantity: this.dessertForm.value.quantity,
      price: this.dessertForm.value.price,
      recipe: {
        ingredients: this.dessertForm.value.ingredients,
      },
    }

    this.store.dispatch(dessertsActions.createDessert({ dessert }))
  }
}
