import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'

import { DashboardComponent } from './components/dashboard/dashboard.component'
import { DashboardRoutingModule } from './dashboard-routing.module'
import { SharedModule } from '../../shared/shared.module'

@NgModule({
  declarations: [DashboardComponent],
  imports: [ReactiveFormsModule, CommonModule, DashboardRoutingModule, SharedModule],
})
export class DashboardModule {}
