import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { IIngredients } from '../../../../core/store/models/dessert.interface'

@Component({
  selector: 'egs-recipe-table',
  templateUrl: './recipe-table.component.html',
  styleUrls: ['./recipe-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecipeTableComponent {
  /**
   * The list of ingredients
   */
  @Input() ingredients: IIngredients[]

  constructor() {}
}
