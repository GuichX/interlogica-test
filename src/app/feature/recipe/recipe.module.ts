import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { RecipeRoutingModule } from './recipe-routing.module'

import { SharedModule } from '../../shared/shared.module'

import { RecipeComponent } from './containers/recipe/recipe.component'
import { RecipeTableComponent } from './components/recipe-table/recipe-table.component'

@NgModule({
  declarations: [RecipeComponent, RecipeTableComponent],
  imports: [CommonModule, RecipeRoutingModule, SharedModule],
})
export class RecipeModule {}
