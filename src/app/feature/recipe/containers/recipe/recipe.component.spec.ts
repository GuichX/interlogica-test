import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'

import { StoreModule } from '@ngrx/store'
import { reducers } from '../../../../reducers'

import { RecipeComponent } from './recipe.component'

describe('RecipeComponent', () => {
  let component: RecipeComponent
  let fixture: ComponentFixture<RecipeComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecipeComponent],
      imports: [StoreModule.forRoot(reducers), RouterTestingModule],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
