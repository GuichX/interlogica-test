import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Observable, Subscription } from 'rxjs'

import { select, Store } from '@ngrx/store'
import { AppState } from '../../../../reducers'

import { IDessert } from '../../../../core/store/models/dessert.interface'
import { dessertsSelectors } from '../../../../core/store/selectors'

@Component({
  selector: 'egs-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],
})
export class RecipeComponent implements OnInit, OnDestroy {
  dessertId: number
  dessert: Observable<IDessert>
  private sub: Subscription

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => (this.dessertId = +params.id))
    this.dessert = this.store.pipe(select(dessertsSelectors.selecDessertDetails(this.dessertId)))
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe()
  }

  handleGoHome(): void {
    this.router.navigateByUrl('/')
  }
}
