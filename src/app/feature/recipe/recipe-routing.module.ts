import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { RecipeComponent } from './containers/recipe/recipe.component'

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RecipeComponent,
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecipeRoutingModule {}
