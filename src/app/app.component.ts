import { Component, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { Store } from '@ngrx/store'
import { dessertsActions } from './core/store/actions'

@Component({
  selector: 'egs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private titleService: Title, private store: Store) {
    titleService.setTitle('Pastry | Interlogica')
  }

  ngOnInit(): void {
    this.store.dispatch(dessertsActions.getDessertsList())
  }
}
