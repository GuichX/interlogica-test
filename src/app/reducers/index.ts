import { ActionReducerMap } from '@ngrx/store'

import { dessertsReducer, userReducer } from '../core/store/reducers'

import { UserState } from '../core/store/reducers/user.reducer'
import { DessertsState } from '../core/store/reducers/desserts.reducer'

/**
 * The initial state of the app
 */
export interface AppState {
  desserts: DessertsState
  user: UserState
}

/**
 * The global reducers of the app
 */
export const reducers: ActionReducerMap<AppState> = {
  desserts: dessertsReducer.reducer,
  user: userReducer.reducer,
}
