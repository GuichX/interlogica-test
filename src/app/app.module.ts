import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms'

import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { EffectsModule } from '@ngrx/effects'

import { environment } from '../environments/environment'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import { reducers } from './reducers'
import { DessertsEffects, UserEffects } from './core/store/effects'

import { SharedModule } from './shared/shared.module'

import { HeaderComponent } from './core/components/header/header.component'
import { DessertComponent } from './core/components/dessert/dessert.component'
import { DessertListComponent } from './core/components/dessert/components/dessert-list/dessert-list.component'
import { LoginComponent } from './core/components/login/login.component'

@NgModule({
  declarations: [AppComponent, HeaderComponent, DessertComponent, DessertListComponent, LoginComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictActionImmutability: true,
        strictStateImmutability: true,
      },
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([DessertsEffects, UserEffects]),
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
